var express = require("express");
var connect = require("connect");
var routes = require("./routes");
var busboy = require("connect-busboy");
var session = require("express-session");

var app = express();
//use the public folder for HTTP requests
app.use(express.static('public'));
app.use(busboy());
//initalise session variables with cookies
app.use(session({
	secret:'s3cr3t',
	saveUninitialized: "true",
	resave: "true"
}));

//-------- Index page routing --------
app.get('/', routes.index);


//-------- Getting items routing --------
app.get('/get_picture/:id', routes.selectPicture);

app.get('/get_all_albums', routes.getAllAlbums);


//-------- Deleting items routing --------
app.get('/delete_album', routes.deleteAlbumGET);

app.get('/delete_picture', routes.deletePictureGET);

app.post('/delete_album', routes.deleteAlbumPOST);

app.post('/delete_picture', routes.deletePicturePOST);


//-------- Admin pages routing --------
app.get('/admin', routes.admin);

app.post('/admin', routes.adminPOST);

app.get('/admin/admin_panel', routes.adminPanel);

app.get('/admin/add_album', routes.addAlbumGET);

app.get('/admin/add_photos', routes.addPhotosGET);

app.post('/admin/add_album', routes.addAlbumPOST);

app.post('/admin/add_picture', routes.addPicturePOST);


//-------- Test routing --------
app.get('/insert_test_album', routes.insertAlbumTest);

app.get('/test_album', routes.insertAlbumTest);

app.get('/test_photo/:albumId', routes.insertPictureTest);

app.get('/select_album', routes.selectTestAlbum);

//-------- End of routing commands --------

console.log("Running at: " + __dirname);
console.log("Listening on port 3000");
app.listen(3000);