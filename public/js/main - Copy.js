/*------------------------------------
jQuary Sliding Effects
blind
bounce
drop
clip
explode
fold
highlight
puff
pulsate
shake
scale
size
slide
------------------------------------*/

$("#main-nav li").on("click", function(e) {

$("#main-nav li").removeClass("active");

$("#"+$(this).prop("id")).addClass("active");

switch($(this).prop("id")){
    
    case "home":    $("#body-container > div").hide("slide", { direction: "right" }, 1000);
                    break;

    case "albums":  if ((!$("#albums-container").is(':visible')) && (!$("#album-photos-container").is(':visible'))) {

                        if ((!$("#about-container").is(':visible')) && (!$("#contact-container").is(':visible')) ) {
                            $("#albums-container").show("slide", { direction: "right" }, 1000);
                        }
                        else{
                            $("#body-container > div").hide("slide", { direction: "right" }, 1000);

                            $("#body-container > div").promise().done(function(){
                                setTimeout(
                                function(){
                                $("#albums-container").show("slide", { direction: "right" }, 1000);
                                }, 1000);
                            });
                        }
                    }
             //   $("#album-photos-container").slideUp( 300 ).delay( 800 ).fadeIn( 800 );
                    break;

    case "about":   if (!$("#about-container").is(':visible')) {

                        if ((!$("#albums-container").is(':visible')) && (!$("#contact-container").is(':visible')) 
                            && (!$("#album-photos-container").is(':visible'))) {

                                $("#about-container").show("slide", { direction: "right" }, 1000);
                        }
                        else{
                            $("#body-container > div").hide("slide", { direction: "right" }, 1000);

                            $("#body-container > div").promise().done(function(){
                                setTimeout(
                                function(){
                                 $("#about-container").show("slide", { direction: "right" }, 1000);
                                }, 1000);
                            });
                        }
                    }
                    break;

    case "contact": if (!$("#contact-container").is(':visible')) {

                        if ((!$("#albums-container").is(':visible')) && (!$("#about-container").is(':visible'))
                            && (!$("#album-photos-container").is(':visible'))) {

                                $("#contact-container").show("slide", { direction: "right" }, 1000);
                        }
                        else{
                            $("#body-container > div").hide("slide", { direction: "right" }, 1000);

                            $("#body-container > div").promise().done(function(){
                                setTimeout(
                                function(){
                                 $("#contact-container").show("slide", { direction: "right" }, 1000);
                                }, 1000);
                            });
                        }
                    }
                    break;

}
});


//--------------------------------------------------------------

$(".album-close-button").on("click", function(e) {

    $("#album-photos-container").hide("slide", { direction: "right" }, 1000);
    $("#album-photos-container").promise().done(function(){
        setTimeout(
        function() 
        {
            $("#albums-container").show("slide", { direction: "right" }, 1000);
        }, 600);

    });

});   

//--------------------------------------------------------------

var albumsDetails;

function getAlbums(e) {
	$.getJSON("/get_all_albums", function(data){
	albumsDetails = {"albums" : data};
 //   console.log(albumsDetails);
	renderTemplates(albumsDetails);
	}); 
}

function renderTemplates(data) {
            var template = $("#albumTemplate").html();

            var renderer = Handlebars.compile(template);

            var result = renderer(data);

    $("#set-albums").html(result); 
}

window.addEventListener("load", getAlbums); 
//  "/get_album/" + album_id 
// var pictures = data.pictures;
//  "/get_picture/" + pictures[i]

//--------------------------------------------------------------
var setAllAlbumPhotos;
var getAllphotoDetails=[];
    var returndata;
var lastItem;


var albumId;

$(document).on('click', '.single-album', function() {

setAllAlbumPhotos = "";
getAllphotoDetails = [];

    albumId = "" + $(this).prop("id");

    $("#body-container > div").hide("slide", { direction: "right" }, 1000);

    $("#body-container > div").promise().done(function(){

        setTimeout(
        function() 
        {
            $("#album-photos-container").show("slide", { direction: "right" }, 1000);
        }, 1000);

    });

    getAlbumName();
    getPhotos();
});

//--------------------------------------------------------------

var photoDetails;

function getPhotos(e) {
	$.getJSON("/get_album/"+albumId, function(data){
	photoDetails = {"singleAlbum" : data};
  //   console.log(photoDetails);
	renderPhotosTemplates(photoDetails);
	}); 

}

function renderPhotosTemplates(data) {
            var template = $("#photoTemplate").html();
            // Handlebars compiles the template into a callable function
            var renderer = Handlebars.compile(template);

            // call the compiled function with the template data
            var result = renderer(data);

    $("#set-album-photos").html(result); 
}



    Handlebars.registerHelper('each_album', function(list, opts) {


        console.log(list.length);

        getPhotoDetailsLoop(list, function(returndata){
            console.log("getAllphotoDetails");
            console.log(getAllphotoDetails);

getAllphotoDetails = {"allAlbumPhotos" : getAllphotoDetails};
getAllphotoDetails = opts.fn(getAllphotoDetails)
      console.log(getAllphotoDetails);

         

        })



   return getAllphotoDetails;
    });

    function getPhotoDetailsLoop(list, callback){
     var i;
     for(i = 0; i < list.length; ++i){

        var picUrl = list[i];

        getPhotoDetials(picUrl ,function(returndata){
    //received data!

    console.log("getAllphotoDetails");
 //   setAllAlbumPhotos = {"allAlbumPhotos" : getAllphotoDetails};   
    console.log("getAllphotoDetails");
    console.log(setAllAlbumPhotos);
    console.log("i = "+ i);
    console.log("list.length = "+ list.length);
    if(lastItem == list[list.length-1]){
        console.log("========================");
     callback(returndata); 
    }

})



    } 
}

function getPhotoDetials(link, callback) {
    $.getJSON("/get_picture/"+link, function(data){
        
        console.log("lastItem = "+ lastItem);
    getAllphotoDetails = getAllphotoDetails.concat(data);
    console.log(getAllphotoDetails);
    lastItem = link;
    callback(returndata);
    }); 
    // console.log("i = " + i);
    //   if(i == list.length){ 
    //     console.log("here");
    //     callback();
    // }
}

//-----------------------------------------------

// Handlebars.registerHelper('each_album', function(list, opts) {


//     console.log(list.length);

//     getAllAlbumPhotos(list, function(data) {
//     console.log("getAllphotoDetails");
//     setAllAlbumPhotos = {"allAlbumPhotos" : getAllphotoDetails};   
//     console.log("getAllphotoDetails");
//     console.log(setAllAlbumPhotos);
//    });

//    return setAllAlbumPhotos;
// });

// function getAllAlbumPhotos(list, callback) {
//     var i;
//     for(i = 0; i < list.length; ++i){

//     var picUrl = "/get_picture/"+list[i];
    
//     $.getJSON(picUrl, function(data){
//     getAllphotoDetails = getAllphotoDetails.concat(data);
//      console.log(getAllphotoDetails);

//     }); 



//     }
//     // console.log("i = " + i);
//     //   if(i == list.length){ 
//     //     console.log("here");
//     //     callback();
//     // }
// }

//-----------------

 //return result;
 //     result = result + opts.fn(list[i]);
//------------------------------------------------------------------------------

// Handlebars.registerHelper('each_album', function(list, opts) {
//     var i, result = '';
//     for(i = 0; i < list.length; ++i)
//         if(list[i].id == albumId)
//             result = result + opts.fn(list[i]);
//     return result;
// });



//window.addEventListener("load", getPhotos); 


//--------------------------------------------------------------

var photoAlbumDetails;

function getAlbumName(e) {
    $.getJSON("/get_album/"+albumId, function(data){
    photoAlbumDetails = {"albumDetails" : data};
     console.log("photoAlbumDetails");
   console.log(photoAlbumDetails);
   renderAlbumNameTemplate(photoAlbumDetails);
    }); 

}

function renderAlbumNameTemplate(data) {
            var template = $("#albumNameTemplate").html();
            
            var renderer = Handlebars.compile(template);

            var result = renderer(data);

    $("#album-details").html(result); 
}


//window.addEventListener("load", getAlbumName); 


//--------------------------------------------------------------


$("#album-photos-container").mouseenter(function() {
     $('#albums-container').perfectScrollbar();


     $('.single-album-photo').magnificPopup({type:'image',
        delegate: 'a', 
        mainClass: 'mfp-with-zoom', 

        zoom: {
            enabled: true, 

            duration: 300, 
            easing: 'ease-in-out', 

            opener: function(openerElement) {
                return openerElement.is('img') ? openerElement : openerElement.find('img');
            }
        }
    });


     
                // $("area[rel^='prettyPhoto']").prettyPhoto();

            // $(".gallery:first a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'normal', show_title:false, autoplay_slideshow: false,allow_resize: false,
            //  default_width: 400,
            // default_height: 300,
            // overlay_gallery: true});

            //     $(".gallery:first a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'normal', show_title:false, autoplay_slideshow: false,allow_resize: false,
            //  default_width: 400,
            // default_height: 300,
            // overlay_gallery: true});
            //     $(".gallery:gt(0) a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'fast',slideshow:10000, show_title:false, hideflash: true,allow_resize: false,
            //  default_width: 400,
            // default_height: 300,
            // overlay_gallery: true});



});

$("#albums-container").mouseenter(function() {
    $('#albums-container').perfectScrollbar();
});