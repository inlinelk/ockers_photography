// This function can be used to pass in an album id and 
// get back all the image NAMES relevant to the album 
//--------------------------------------------------------------

function getAllImageNamesByAlbumObject(albumObj, callback) {
    console.log("from utils.js: ");
    console.log(albumObj);
   //  console.log(albumObj.length);
    var allNames = [];
    var i;
    for(i = 0; i < albumObj.pictures.length; i++) {
        $.getJSON('/get_picture/' + albumObj.pictures[i], function(data) {
            console.log("data from /get_picture/");
            console.log(data);
            allNames[i] = data[0].title;  
            //wait till all the image names have been fed to the array, then callback
            console.log("i : " + i);
            console.log(albumObj.pictures.length);
              console.log(allNames[i]);
            if(i == (albumObj.pictures.length)) {
                console.log("ending loop");
                callback(allNames);    
            }
        }); 
        }
}

//--------------------------------------------------------------

/*
{
    {
        name: "Lorem",
        pictures: []
    },
    {
        name: "Lorem",
        pictures: []
    },
}
*/
