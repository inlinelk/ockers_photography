/*------------------------------------------
Database access object for the album 
collection. 

Able to set the required 
fields for insertion and use other functions
such as delete and getting records
-------------------------------------------*/

//-------- Document field setters --------
exports.setName = function(name) {
    this.name = name;
}

exports.setDescription = function(des) {
    this.description = des;
}

exports.setAlbumImage = function(img) {
    this.albumImage = img;
}

exports.setShortName = function(name) {
    this.shortName = name;
}

exports.setPicture = function(pic) {
    this.picture = pic;
}

exports.setAlbumUrl = function(u) {
    this.url = u;   
}

//-------- CRUD functions --------

//this is an async function
//async architecture was used here so that the callback function would be 
//executed only after the database query had been run
exports.insertRecord = function(dbObj, callback) {
    //create the document
    this.document = {
        name: this.name,
        description: this.description,
        album_image: this.albumImage,
        short_name: this.shortName,
        url : this.url,
        pictures: []
    };
    
    //run the query
    dbObj.album.insert(this.document, function(err, record) {   
        console.log("err: " + err);
        console.log("record: " + record);
        console.log("record._id: " + record._id);
        
        if(err) {
            callback(false, err);
        }
        callback(true, record._id);
    });
}

exports.getRecord = function(dbObj, key, callback) {
    
    if(key != null) {
        this.criteria = {_id: dbObj.ObjectId(key)};
    }
    else {
        this.criteria = {};
    }
    
    dbObj.album.find(this.criteria, function(err, recordSet) {
     
        if(err) {
            callback(false, err);
        }
        callback(true, recordSet);
        
    });
}

//this function will add a picture record to the passed in album id
exports.addPicture = function(dbObj, key, pic, callback) {
    
    //db.collection.update(query, update, [options], [callback])
    dbObj.album.update(
        {_id:dbObj.ObjectId(key)},
        {$push : {pictures: pic}},
        {},
        function(err, response) {
            if(err) {
                callback(false, err);
            }
            callback(true, response);
        });
}

exports.deleteRecord = function(dbObj, key, callback) {
    this.criteria = {_id:dbObj.ObjectId(key)};
    
    dbObj.album.remove(this.criteria, function(err, result, lastError) {
        console.log("err: " + err);
        console.log("result: " + result);
        console.log("lastError: " + lastError);

        if(err) {
            callback(false, err);
        }
        callback(true, result);
    });   

}
