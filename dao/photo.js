/*------------------------------------------
Database access object for the photo 
collection. 

Able to set the required 
fields for insertion and use other functions
such as delete and getting records
-------------------------------------------*/

//-------- Document field setters --------
exports.setTitle = function(t) {
    this.title = t;   
}

exports.setName = function(n) {
    this.name = n;   
}

exports.setDescription = function(d) {
    this.description = d;   
}

exports.setUrl = function(u) {
    this.url = u;   
}

exports.setPictureThumb = function(t) {
    this.thumb = t;
}

exports.setPictureThumbUrl = function(t) {
    this.thumbUrl = t;
}

//-------- CRUD functions --------
exports.insertRecord = function(dbObj, callback) {
 
    this.document = {
        name : this.name,
        title : this.title,
        description : this.description,
        url : this.url,
        thumbnail : this.thumb,
        thumbnail_url : this.thumbUrl
    };
    
    dbObj.photo.insert(this.document, function(err, record) {
        if(err) {
            callback(false, err);   
        }
        callback(true, record._id);
    });
},
    
exports.getRecord = function(dbObj, key, callback) {
 
    if(key != null) {
        this.criteria = {_id:dbObj.ObjectId(key)};
    }
    else {
        this.criteria = {};   
    }
    
    dbObj.photo.find(this.criteria, function(err, recordSet) {
       
        if(err) {
            callback(false, err);   
        }
        callback(true, recordSet);
        
    });
},

exports.deleteRecord = function(dbObj, key, callback) {
    this.criteria = {_id:dbObj.ObjectId(key)};
    
    dbObj.photo.remove(this.criteria, function(err, result, lastError) {
        console.log("err: " + err);
        console.log("result: " + result);
        console.log("lastError: " + lastError);

        if(err) {
            callback(false, err);
        }
        callback(true, result);
    });   

}