//define and set database variables
var mongojs = require("mongojs");
var dbUri = "mongodb://localhost/ockers_photography";
var db = mongojs.connect(dbUri, ["album", "photo"]);

//other variables
var album = require("./dao/album.js");
var picture = require("./dao/photo.js");
var Busboy = require("connect-busboy");
var os = require("os");
var path = require("path");
var fs = require("fs");

//-------- Index page routing --------
exports.index = function(req, res) {
    res.sendfile("./public/views/index.html");
},   

//-------- Admin functions routing - Static --------    
exports.admin = function(req, res) {
    res.sendfile("./public/views/admin/index.html");
},
    
exports.adminPanel = function(req, res) {
    if(!req.session.loggedIn) {
        res.status(500).sendfile("./public/views/admin/500.html");
    }
    else {
        res.status(200).sendfile("./public/views/admin/adminPanel.html");    
    }
},

exports.addAlbumGET = function(req, res) {
    if(!req.session.loggedIn) {
        res.status(500).sendfile("./public/views/admin/500.html");
    }
    else {
        res.status(200).sendfile("./public/views/admin/addAlbum.html");    
    }
},

exports.addPhotosGET = function(req, res) {
    if(!req.session.loggedIn) {
        res.status(500).sendfile("./public/views/admin/500.html");
    }
    else {
        res.status(200).sendfile("./public/views/admin/addPhotos.html");    
    }
}
    
exports.deleteAlbumGET = function(req, res) {
    if(!req.session.loggedIn) {
        res.status(500).sendfile("./public/views/admin/500.html");
    }
    else {
        res.status(200).sendfile('./public/views/admin/deleteAlbum.html');    
    }
},

exports.deletePictureGET = function(req, res) {
    if(!req.session.loggedIn) {
        res.status(500).sendfile("./public/views/admin/500.html");
    }
    else {
        res.status(200).sendfile('./public/views/admin/deletePhoto.html');    
    }
},


//-------- Admin functions routing - Non-Static -------- 
exports.adminPOST = function(req, res) {
    req.pipe(req.busboy);
    var username;
    var password;

    req.busboy.on('field', function(fieldname, value) {
        console.log(fieldname + " : " + value);
        if(fieldname == "username") {
            username = value;
        }
        else if(fieldname == "password") {
            password = value;

            if(username == "admin" && password == "admin123") {
                console.log("logging in user");
                req.session.loggedIn = true;
                console.log(req.session);
                res.status(200).sendfile('./public/views/admin/adminPanel.html');            
            }
            else {
                res.sendfile("./public/views/admin/500.html");
            }
        }

    });
},

exports.addAlbumPOST = function(req, res) {
    console.log("album post request received");
    req.pipe(req.busboy);
    
    var uploadDirectory;
    var albumShortName;
    
    req.busboy.on('field', function(fieldname, value) {
        if(fieldname == "album-short-name") {   
            console.log("album short name found");
            //set the album shortname variable
            album.setShortName(value);
            albumShortName = value;
            uploadDirectory = path.join(__dirname + "/public/images/albums/", value);  

            uploadDirectory = path.normalize(uploadDirectory);

        }
        else if(fieldname == "album-name") {
            album.setName(value);
        }
        else if(fieldname == "album-description") {
            album.setDescription(value);
        }
        //console.log("Fieldname: " + fieldname);
    });
    
    //Upload the album image
    req.busboy.on('file', function(fieldname, file, filename) {
        console.log("Fieldname: " + fieldname);
        console.log("Filename: " + filename);
        //uploadDirectory = path.join(__dirname + "/public/images/albums/", value);
        
        if(!fs.exists(uploadDirectory)) {
            fs.mkdir(uploadDirectory, function(err) {
                            console.log(uploadDirectory);
                if(err) {
                    res.status(500).json(err); 
                }
                else {
                    var ostream = fs.createWriteStream(path.join(uploadDirectory, filename));
                    //console.log("file exists: " + fs.existsSync(uploadDirectory));
                    album.setAlbumUrl(uploadDirectory);
                    file.pipe(ostream);
                    ostream.on('close', function(err) {
                        if(err) {
                            res.status(500).json(err);
                        }
                        else {
                            album.setAlbumImage(filename);

                            album.insertRecord(db, function(result, message) {
                                if(result == false) {
                                    res.status(500).json(message);
                                }
                                else {
                                    res.status(200).json({message:"album created successfully"});   
                                }
                            });
                        }
                    });
                }
            });   
        }
    });   

},
    
exports.addPicturePOST = function(req, res) {
    //console.log("received picture post request");
    req.pipe(req.busboy);
    
    var albumId;
    var albumUrl;
    var thumbUrl;
    var thumbsObject = {};
    var pictureObject = {};
    
    req.busboy.on('field', function(fieldname, value) {
        if(fieldname == "picture-title") {
            picture.setTitle(value);
            console.log("1");
        }
        else if(fieldname == "picture-description") {
            console.log("description: " + value);
            picture.setDescription(value);
            console.log("2");
        }
        else if(fieldname == "album-id") {
            console.log("album-id: " + value);
            albumId = value;
            console.log("3");
        }
        else if(fieldname == "album-url") {
            albumUrl = value;
            console.log("album-url: " + value);
            console.log("4");
        }
    });
    
    req.busboy.on('file', function(fieldname, file, filename) {
        console.log("file: " + file);
        
        if(fieldname == "picture-thumb") {
            
            var temp = path.join(albumUrl, "/thumb");
            
            temp =  path.normalize(temp);

            picture.setPictureThumbUrl(temp);
            picture.setPictureThumb(filename);
            console.log("thumb path: " + temp);
            fs.exists(temp, function(result) {
                if(result == false) {
                    console.log("temp folder doesnt exist");
                    fs.mkdir(temp, function(err) {
                        if(err) {
                            res.status(500).json(err);   
                            console.log(err);
                        }
                    });
                }
                
                var ostream = fs.createWriteStream(path.join(temp, filename));
                file.pipe(ostream); 
            });
        }
        else if(fieldname == "picture") {
            
            var temp = path.join(albumUrl, filename);
            picture.setUrl(temp);
            picture.setName(filename);
            var ostream = fs.createWriteStream(temp);
            file.pipe(ostream);
            ostream.on('close', function(err) {
                if(err) {
                    res.status(500).json(err);   
                }
                
                var result = picture.insertRecord(db, function(result, message) {
                    if(result == false) {
                        res.status(500).json(message);   
                    }
                    else {
                        //res.status(200).json({status:"success", message:message});  
                        console.log("pictureId: " + message);
                        console.log("albumId: " + albumId);
                        this.pictureId = message;
                        result = album.addPicture(db, albumId, message, function(result, message) {
                            if(result == false) {
                                res.status(500).json(message);   
                            }
                        });
                    }    
                });   
                
                res.status(200).json({message:"picture was saved successfully"});
            });
        }
        
    });
},

exports.deletePicturePOST = function(req, res) {
    console.log("request to delete picture received");
    req.pipe(req.busboy);
    
    req.busboy.on("field", function(fieldname, value) {
        //console.log(fieldname + " : " + value);
        if(fieldname == "picture-id") {
            picture.deleteRecord(db, value, function(result, message) {
                if(result == false) {
                    res.status(500).json(message);
                }
                else {
                    res.status(200).json({message:"picture deleted successfully"});
                }
            });
        }
    });
},

exports.deleteAlbumPOST = function(req, res) {
    console.log("request to delete album received");
    req.pipe(req.busboy);
    
    req.busboy.on("field", function(fieldname, value) {
        // console.log(fieldname + " : " + value);
        if(fieldname == "album-id") {
            album.deleteRecord(db, value, function(result, message) {
                if(result == false) {
                    res.status(500).json(message);
                }
                else {
                    res.status(200).json({message:"album deleted successfully"});
                }
            });
        }
    });
},
  
//-------- General functions routing --------  
exports.getAlbum = function(req, res) {
 
    this.requestId = req.param("id");
    
    album.getRecord(db, this.requestId, function(result, message) {
        if(result ==  false) {
            res.status(500).json(message);   
        }
        else {
            res.status(200).json(message);   
        }
    });
    
},

exports.getAllAlbums = function (req, res) {
 
    album.getRecord(db, null, function(result, message) {
        if(result ==  false) {
            res.status(500).json(message);   
        }
        else {
            res.status(200).json(message);   
        }
    });   
},


exports.selectPicture = function(req, res) {
    
    this.requestId = req.param("id");
    
    picture.getRecord(db, this.requestId, function(result, message) {
        if(result == false) {
            res.status(500).json(message);   
        }
        else {
            res.status(200).json(message);   
        }
    });
    
},

//-------- Test functions routing --------  
exports.insertAlbumTest = function(req, res) {
    album.setName("test name");
    album.setDescription("test description");
    
    album.insertRecord(db, function(result, message) {
        if(result == false) {
            res.status(500).json(message);
        }
        else {
            res.status(200).json(message);
        }
    });  
},

exports.insertPictureTest = function(req, res) {
    picture.setName("test picture");
    picture.setDescription("test description");
    picture.setUrl("/images/image01.png");
    this.pictureId = req.param("albumId");
    
    var result = picture.insertRecord(db, function(result, message) {
        if(result == false) {
            res.status(500).json(message);   
        }
        else {
            //res.status(200).json({status:"success", message:message});   
            result = album.addPicture(db, this.pictureId, message, function(result, message) {
                if(result == false) {
                    res.status(500).json(message);   
                }
                else {
                    res.status(200).json(message);   
                }
            });
        }    
    });    
},

exports.selectTestAlbum = function(req, res) {
 
    album.getRecord(db, null, function(result, message) {
        if(result ==  false) {
            res.status(500).json({status:"fail", message:message});   
        }
        else {
            res.status(200).json({status:"success", message:message});   
        }
    });
}